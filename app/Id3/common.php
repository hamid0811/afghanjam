<?php
namespace App\Id3;
class common
{
    public static function add_server_paths_to_files($songs){
        $fullMp3path = 'https://afghanjam.ams3.digitaloceanspaces.com/medias/mp3/';
        $fullImagePath = 'https://afghanjam.ams3.digitaloceanspaces.com/images/mp3/';
        $songs = $songs;

        foreach ($songs as $key => $song) {
            $song->url = $fullMp3path.$song->url;
            $song->cover = $fullImagePath.$song->cover;
        }

        return $songs;
    }
}
