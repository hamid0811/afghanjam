<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;


class AttachmentController extends Controller
{

    //////////////////////////Load Uploaded songs/////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    public function index(request $request)
    {
        $i = 0;
        $getID3 = new \getID3;

        $songsData = array();

        $songs = Storage::files('public/uploads');

        foreach ($songs as $song) {
            $filename = $song;

            $song = storage_path('app/'.$song);

            $ThisFileInfo = $getID3->analyze($song);
            $title =  $ThisFileInfo['tags']['id3v2']['title'][0];
            $artist =  $ThisFileInfo['tags']['id3v2']['artist'][0];
            $album =  $ThisFileInfo['tags']['id3v2']['album'][0];
            $filename =  $ThisFileInfo['filename'];
            $Image='data:'.$ThisFileInfo['comments']['picture'][0]['image_mime'].';charset=utf-8;base64,'.base64_encode($ThisFileInfo['comments']['picture'][0]['data']);

            $songsData[$i]['title'] = $title;
            $songsData[$i]['artist'] = $artist;
            $songsData[$i]['filename'] = $filename;
            $songsData[$i]['album'] = $album;

            $source = fopen($Image, 'r');
            Storage::put('public/'.$title.'.jpg', $source);


            $songsData[$i]['image'] = $title.'.jpg';
            fclose($source);
            $i++;
        }

        $artists = DB::select('select * from artists');
        $artistId = $request->input('artist');
        if($artistId !== null){
            $artistId = $request->input('artist');
        }else{
            $artistId = 'No Album';
        }

        $albums = DB::select('select * from albums where artist_id = (?)', [$artistId]);

        return ($songsData);
    }


    ///////////////////////////////create new artist/////////////////////////
    /////////////////////////////////////////////////////////////////////////

    public function create(Request $request)
    {
        $artistName = $request->input('name');
        $imageName = uniqid('img_');
        $path = $request->file('image')->storeAs('images/artists/', $imageName.'.jpg' , 'do_spaces');
        DB::insert('insert into artists (name, photo) values (?,?)', [$artistName, $imageName.'.jpg']);
        return ('Artist '. $artistName.' has been created.');
    }


    ////////////////////Store song infomration into the database///////////////
    ///////////////////////////////////////////////////////////////////////////

    public static function store($artistID,  $albumID = null)
    {
        $artistID = $artistID;
        $albumID = $albumID;
        $i = 0;
        $getID3 = new \getID3;

        $songsData = array();

        $songs = Storage::files('public/uploads');

        foreach ($songs as $song) {

            $relativePath = $song;
            $song = storage_path('app/'.$song);
            $songSource = fopen($song, 'r');


            $ThisFileInfo = $getID3->analyze($song);
            // die(var_dump($ThisFileInfo));


            $title =  $ThisFileInfo['tags']['id3v2']['title'][0];
            $artist =  $ThisFileInfo['tags']['id3v2']['artist'][0];
            $album =  $ThisFileInfo['tags']['id3v2']['album'][0];
            $filename =  $ThisFileInfo['filename'];
            $Image='data:'.$ThisFileInfo['comments']['picture'][0]['image_mime'].';charset=utf-8;base64,'.base64_encode($ThisFileInfo['comments']['picture'][0]['data']);

            $songsData[$i]['title'] = ucwords($title);
            $songsData[$i]['artist'] = ucwords($artist);
            $songsData[$i]['filename'] = $filename;
            $songsData[$i]['album'] = ucwords($album);


            $source = fopen($Image, 'r');

            $imageName = uniqid('img_');
            $trackName = uniqid('track_');

            Storage::disk('do_spaces')->put('images/mp3/'.$imageName.'.jpg', $source, 'public');
            Storage::disk('do_spaces')->put('medias/mp3/'.$trackName.'.mp3', $songSource, 'public');
            fclose($songSource);

            Storage::delete('public/'.$title.'.jpg');
            Storage::delete($relativePath);

            DB::insert(
                'insert into tracks
                (title, artist_id, album_id, albumName, artist, cover, lyrics, url)
                values (?,?,?,?,?,?,?,?)',
                [ucwords($title),$artistID, $albumID, ucwords($album), ucwords($artist), $imageName.'.jpg','',$trackName.'.mp3']
            );



            $i++;
        }

       return ('All songs uploaded');
    }


    /////////////////////////////////Create a new album///////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    public function storeAlbum(request $request){
        $artistID = $request->get('artistID');
        $albumName = $request->input('name');

        $imageName = uniqid('img_');
        $request->file('cover')->storeAs('images/albums/', $imageName.'.jpg' , 'do_spaces');
        DB::insert('insert into albums (artist_id, name, cover) values (?,?,?)', [$artistId, $albumName, $imageName.'.jpg']);
        return ('Album '. $albumName.' has been created.');
    }


    public function getAlbum(request $request){
        $artistId = $request->input('artist');

        $albums = DB::select('select * from albums where artist_id = (?)', [$artistId]);
        $artists = DB::select('select * from artists');

        return redirect()->back()->with(compact('albums', 'artists'));
    }

    public function delete(request $request){
        Storage::delete('public/uploads/'. $request->get('name'));
        Storage::delete('public/'. $request->get('image'));
        return $request;
    }

}
