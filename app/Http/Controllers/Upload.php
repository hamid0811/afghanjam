<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \App\Http\Controllers\AttachmentController;
use Illuminate\Support\Facades\DB;

class Upload extends Controller
{
    public function upload (Request $request)
    {
        $artistID = $request->get('artistID');
        $albumID = $request->get('albumID');

	    if($files=$request->file('files')){
	        foreach($files as $file){
	            $name= $file->getClientOriginalName();

	            $path = Storage::putFileAs('public/uploads', $file, $name);
                AttachmentController::store($artistID, $albumID);

	        }
	    }




	    return ($request);
    }


    public function show(){
    	$allFiles = [];
    	$i= 0;
    	$files = Storage::files('uploads');

    	foreach ($files as $file) {
    		$pieces = explode('/', $file);
    		$allFiles[$i] = $pieces[1];
    		$i++;
    	}

    	return view('show', compact('allFiles'));
    }
}
