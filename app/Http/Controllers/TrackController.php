<?php

namespace App\Http\Controllers;

use App\DOServers\Filepaths;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $track = DB::table('tracks')->where('id', $id)->get();
        // $track = Filepaths::add_server_paths_to_files($track);

        // if($track[0]->releaseDate){
        //   $date = new Carbon($album[0]->releaseDate);
        //     $album[0]->releaseDate = $date->year;
        // }



        $tracks = DB::table('tracks')->where('id', $id)->get();
        $tracks = Filepaths::add_server_paths_to_files($tracks);

        return Response::json($tracks);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('tracks')->where('id', $id)->increment('play_count');
        // DB::table('albums')->where('id', $request->get('album_id'))->increment('play_count');


        $date = new \DateTime();
        $song = DB::table('trendings')->where('track_id', $id)->get();

        if(!$song->isEmpty()){
            DB::table('trendings')->where('track_id', $id)->increment('play_count');
        }else{
           DB::insert('insert into trendings (track_id, date, play_count) values (?, ?, 1)', [$id, $date]);
        }

        return ('play count has been incremented');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $track = DB::table('tracks')->find($id);
        DB::table('tracks')->where('id', $id)->delete();
        // dd($track);
        Storage::disk('do_spaces')->delete('/medias/mp3/'.$track->url);

        return('Track has been deleted');
    }
}
