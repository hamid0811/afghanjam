<?php

namespace App\Http\Controllers;

use App\DOServers\Filepaths;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Response;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $artistID = $request->get('artistID');
        $albumName = $request->input('name');
        $releaseDate = $request->get('releaseDate');

        $imageName = uniqid('img_');
        $request->file('cover')->storeAs('images/albums/', $imageName.'.jpg' , 'do_spaces');
        DB::insert('insert into albums (artist_id, name, cover, releaseDate) values (?,?,?,?)', [$artistID, $albumName, $imageName.'.jpg', $releaseDate]);
        $albumID = DB::getPdo('albums')->lastInsertId();

        return ($albumID);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $album = DB::table('albums')->where('id', $id)->get();
        $album = Filepaths::albums_content_endpoint($album);

        if($album[0]->releaseDate){
          $date = new Carbon($album[0]->releaseDate);
            $album[0]->releaseDate = $date->year;
        }



        $tracks = DB::table('tracks')->where('album_id', $id)->get();
        $tracks = Filepaths::add_server_paths_to_files($tracks);

        return Response::json([$tracks, $album]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tracks = DB::table('tracks')->where('album_id', $id)->get();
        foreach ($tracks as $track) {
            Storage::disk('do_spaces')->delete('medias/mp3/'.$track->url);
        }
        DB::table('tracks')->where('album_id', $id)->delete();
        DB::table('albums')->where('id', $id)->delete();

        return('Album been deleted');
    }
}
