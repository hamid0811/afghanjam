<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\DOServers\Filepaths;

class TrendingController extends Controller
{

    public function index()
    {
       $trendingSongs = DB::select('select tracks.*
                                    from tracks
                                    inner join trendings on tracks.id = trendings.track_id
                                    where trendings.date
                                    between date_sub(now(),INTERVAL 1 WEEK) and now() ORDER BY play_count DESC LIMIT 12');
       $trendingSongs = Filepaths::add_server_paths_to_files($trendingSongs);
       return $trendingSongs;
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function incrementPlay($id)
    {
        DB::table('tracks')->whereId($id)->increment('playCount');

        $date = new \DateTime();
        $song = DB::table('trendings')->where('song_id', $id)->get();

        if(!$song->isEmpty()){
            DB::table('trendings')->where('song_id', $id)->increment('playCount');
        }else{
           DB::insert('insert into trendings (song_id, date, playCount) values (?, ?, 1)', [$id, $date]);
        }
        return "Play has been incremented";
    }


    public function destroy($id)
    {
        //
    }
}
