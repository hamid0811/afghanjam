<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\DOServers\Filepaths;

class SearchController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($query)
    {
        $artists = DB::table('artists')
                    ->where('name', 'like', $query.'%')
                    ->get();
        $artists = Filepaths::artists_content_endpoint($artists);
        return $artists;
    }

    public function searchSongs($query){
        $songs = DB::table('tracks')
                    ->where('title', 'like', $query.'%')
                    ->get();
        $songs = Filepaths::add_server_paths_to_files($songs);

        return $songs;
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
