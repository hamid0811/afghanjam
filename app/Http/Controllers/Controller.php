<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function add_server_paths_to_files($songs){
        $fullMp3path = 'https://afghanjam.ams3.digitaloceanspaces.com/medias/mp3/';
        $fullImagePath = 'https://afghanjam.ams3.digitaloceanspaces.com/images/mp3/';

        $songs = $songs;

        foreach ($songs as $key => $song) {
            $song->url = $fullMp3path.$song->url;
            $song->cover = $fullImagePath.$song->cover;
        }
        return $songs;
    }
}
