<?php

namespace App\Http\Controllers;

use App\DOServers\Filepaths;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Response;


class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artist = DB::table('artists')->where('id', $id)->get();
        $artist = Filepaths::artists_content_endpoint($artist);

        $albums = DB::table('albums')->where('artist_id',$id)->get();
        $albums = Filepaths::albums_content_endpoint($albums);

        $singles = DB::table('tracks')->where([['artist_id', '=', $id], ['album_id', '=', null]])->get();
        $singles = Filepaths::add_server_paths_to_files($singles);

        foreach ($albums as $album) {
            if($album->releaseDate){
                $date = new Carbon($album->releaseDate);
                $album->releaseDate = $date->year;
            }
        }

        foreach ($singles as $single) {
            if($single->releaseDate){
                $date = new Carbon($single->releaseDate);
                $single->releaseDate = $date->year;
            }
        }

        $PopularTracks = DB::table('tracks')->where('artist_id', $id)->orderBy('play_count', 'desc')->limit(5)->get();
        $PopularTracks = Filepaths::add_server_paths_to_files($PopularTracks);


        $data = ['artist'=> $artist[0], 'albums'=> $albums, 'singles'=> $singles, 'popular' =>$PopularTracks];

        return Response::json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showAlbums($id)
    {
        $albums = DB::table('albums')->where('artist_id',$id)->get();
        $albums = Filepaths::albums_content_endpoint($albums);
        return Response::json($albums);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
