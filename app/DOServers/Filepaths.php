<?php
namespace App\DOServers;
class FilePaths
{
    public static function add_server_paths_to_files($songs){
        $fullMp3path = 'https://afghanjam.ams3.digitaloceanspaces.com/medias/mp3/';
        $fullImagePath = 'https://afghanjam.ams3.digitaloceanspaces.com/images/mp3/';
        $songs = $songs;

        foreach ($songs as $key => $song) {
            $song->url = $fullMp3path.$song->url;
            $song->cover = $fullImagePath.$song->cover;
        }

        return $songs;
    }

    public static function albums_content_endpoint($albums){
        $albumCoverEndpoint = 'https://afghanjam.ams3.digitaloceanspaces.com/images/albums/';
        $albums = $albums;

        foreach ($albums as $key => $album) {
            $album->cover = $albumCoverEndpoint.$album->cover;
        }

        return $albums;
    }

    public static function artists_content_endpoint($artists){
        $artistsPhotoEndpoint = 'https://afghanjam.ams3.digitaloceanspaces.com/images/artists/';
        $artists = $artists;

        foreach ($artists as $key => $artist) {
            $artist->photo = $artistsPhotoEndpoint.$artist->photo;
        }

        return $artists;
    }
}
