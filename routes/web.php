<?php

use Illuminate\Http\Request;
use \App\DOServers\Filepaths;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;



Route::get('/login', function() {
    return view('home');
});

Route::post('/upload', 'Upload@upload')->middleware('admin');
Route::get('/show', 'AttachmentController@index');
Route::post('/artists', function (request $request) {
	return $request;
});

Route::post('/store','AttachmentController@store');
Route::post('/create','AttachmentController@create');
Route::post('/createAlbum','AttachmentController@storeAlbum');
Route::post('/getAlbum','AttachmentController@getAlbum');
Route::post('/delete/','AttachmentController@delete');

Route::get('/allsongs', function () {
    $songs = DB::table('tracks')->orderBy('id', 'desc')->limit(25)->get();

    $songs = Filepaths::add_server_paths_to_files($songs);
    return $songs;
});

Route::get('/api/artists/{query}', 'SearchController@show');
Route::get('/api/search/songs/{query}', 'SearchController@searchSongs');

Route::get('/songs/{id}', function ($id) {
    $song = DB::table('tracks')->find($id);
    return Response::json($song);
});

Route::get('/api/songs/artist/{id}', function ($id) {
    $users = DB::table('tracks')
                ->where('artist_id', '=', $id)
                ->get();

    return $users;
});

Route::get('/api/artist/{id}', 'ArtistController@show');

Route::get('/api/artist/{id}/singles', 'SinglesController@show');


Route::get('/api/increascount/{id}', 'TrendingController@incrementPlay');
Route::get('/api/trending', 'TrendingController@index');

Route::get('/api/album/{id}', 'AlbumController@show');
Route::post('/albums/store', 'AlbumController@store')->middleware('admin');
Route::delete('api/album/{id}', 'AlbumController@destroy')->middleware('admin');

Route::post('api/upload/singles', 'SinglesController@store');

Auth::routes();

Route::delete('/api/track/{id}', 'TrackController@destroy')->middleware('auth');
Route::patch('/api/track/{id}', 'TrackController@update');
Route::get('/api/track/{id}', 'TrackController@show');

Route::get('/api/user/getCurrentTrack', 'CurrentlyPlayingTrackController@index');
Route::get('/api/user/{id}', 'CurrentlyPlayingTrackController@update');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/manage/{vue_capture?}', function () {
    return view('editsongs');
})->where('vue_capture', '[\/\w\.-]*')->middleware('admin');

Route::get('/{vue_capture?}', function () {
    return view('welcome');
})->where('vue_capture', '[\/\w\.-]*');
