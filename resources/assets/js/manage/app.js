require('./bootstrap');
window.Vue = require('vue');
window.VueRouter = require('vue-router').default;
import BootstrapVue from 'bootstrap-vue'

import App from'./App.vue';
import Routes from './routes';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter);
Vue.use(BootstrapVue);
window.Event = new Vue();


const router = new VueRouter({
    mode:'history',
    base:'/manage',
    fallback: true,
    routes: Routes
});

var eventHub = new Vue();

const app = new Vue({
    el: '#app',
    router,
    components:{
        'App': App,
    }
    // render: h => h(App, Player),
});
