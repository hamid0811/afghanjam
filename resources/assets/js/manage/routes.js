import SearchPage from './components/SearchPage.vue';
import ArtistPage from './components/artists/ArtistPage.vue';
import AlbumPage from './components/albums/AlbumPage.vue';

export default [
  { path: '/', component: SearchPage},
  { path: '/artist/:id/', component: ArtistPage, props: true },
  { path: '/album/:id/', component: AlbumPage, props: true},
]
