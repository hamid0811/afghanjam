require('./bootstrap');
window.Vue = require('vue');
window.VueRouter = require('vue-router').default;
import 'babel-polyfill';

import App from'./App.vue';
import Routes from './routes';
import BootstrapVue from 'bootstrap-vue'
import { store } from './store.js';

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter);
Vue.use(BootstrapVue);
window.Event = new Vue();


const router = new VueRouter({
    mode:'history',
    base:'/',
    fallback: true,
    routes: Routes
});

var eventHub = new Vue();

const app = new Vue({
    el: '#app',
    store,
    router,
    components:{
        'App': App,
    }
});
