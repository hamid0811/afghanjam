import SearchPage from './components/SearchPage.vue';
import Artist from './components/artists/Artist.vue';
import AlbumPage from './components/albums/AlbumPage.vue';
import HomePage from './components/home/HomePage.vue';
import TrackPage from './components/tracks/TrackPage.vue';
import AuthPage from './components/auth/AuthPage.vue';

export default [
    { path: '/', component: HomePage},
    { path: '/search', component: SearchPage},
    { path: '/artist/:id/', component: Artist, props: true },
    { path: '/album/:id/', component: AlbumPage, props: true},
    { path: '/track/:id/', component: TrackPage, props: true},
    { path: '/vuelogin/', component: AuthPage}
]
