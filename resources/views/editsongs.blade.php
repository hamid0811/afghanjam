<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body>

    <div id="app">
        <App></App>
    </div>

    <script src="/js/app.js" charset="UTF-8"></script>
</body>
</html>
