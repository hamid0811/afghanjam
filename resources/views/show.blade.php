<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Show Files</title>
	<link rel="stylesheet" href="css/app.css">
</head>
<body>

	<div id="app">
        <example-component></example-component>
        <upload-component></upload-component>
    </div>

<div class="container">
	<div class="row justify-content-center">
        <div class="col-md-12">

			<form action="/store" method="POST" enctype="multipart/form-data">
				@foreach ($songsData as $file)
					<input type="text"  name="{{$file['artist']}}" value="{{$file['artist']}}">
					<input type="text"  name="{{$file['title']}}" value="{{$file['title']}}">
					<input type="text" name="{{$file['album']}}" value="{{$file['album']}}">
					<input type="text" name="{{$file['filename']}}" value="{{$file['filename']}}"><br>
				@endforeach
			  <input type="submit" value="Submit">
			</form>

        </div>
    </div>
</div>


	<script src="js/app.js" charset="UTF-8"></script>

</body>
</html>
