<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#212529">
    <link rel="stylesheet" type="text/css" href="/css/main.css">
</head>
<body>
    <style type="text/css">
        body
        {
            font-family: 'Muli', sans-serif;
         }
    </style>

    <div id="app">
        <App></App>
    </div>

    <script src="/js/main.js" charset="UTF-8"></script>
</body>
</html>


